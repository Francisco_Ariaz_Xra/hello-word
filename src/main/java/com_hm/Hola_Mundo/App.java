package com_hm.Hola_Mundo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.Mundo.Mundo;

public class App {
	
	public static void main(String[] args ) {
			
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/mixml/beans.xml");
		Mundo m = (Mundo) appContext.getBean("mundo");
		
		System.out.println(m.getSaludo());

	}	
}
